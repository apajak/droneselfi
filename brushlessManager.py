## Ypets PROJECT ##
## code for manage Brushless-motors ##
# Alexandre Pajak 16/06/2022
# -- coding: utf-8 --
from threading import Thread
from time import sleep
import os

try:
    os.system("sudo killall pigpiod")
except Exception:
    print("sudo killall error")
try:
    os.system("sudo pigpiod")  # Launching GPIO library
except Exception:
    print("sudo pigpiod error")
sleep(1)
try:
    import pigpio  # importing GPIO library
except Exception:
    print("pigpio module error")


class ESC:
    ## YMMV
    MIN_WIDTH = 1170
    # Note that some batteries, 12 volt PSUs, etc. might only be capable of far less than this (e.g. 1350)
    # However, the controllers range should still be set to max for finest full-scale resolution.
    MAX_WIDTH = 1500
    # MAX_WIDTH: int = int(1350)
    NEW_WIDTH: int = int(1170)
    CURRENT_WIDTH: int = int(1240)
    STOP = False

    def __init__(self, pin, name):
        self.conn = pigpio.pi()
        self.pin = pin
        self.name = name

    def pwm(self, width: int, snooze: int = 0) -> None:
        """
        Convenience wrapper around pigpio's set_servo_pulsewidth.
        width: The servo pulsewidth in microseconds. 0 switches pulses off.
        snooze: seconds before returning.
        """
        # print("Setting pulse width to", width, "microseconds for", snooze, "seconds.")
        self.conn.set_servo_pulsewidth(self.pin, width)
        if snooze:
            sleep(snooze)
        return

    def calibrate(self) -> None:
        """
        This trains the ESC on the full scale (max - min range) of the controller / pulse generator.
        This only needs to be done when changing controllers, transmitters, etc. not upon every power-on.
        NB: if already calibrated, full throttle will be applied (briefly)!  Disconnect propellers, etc.
        """
        print("Calibrating...")
        self.pwm(width=self.MAX_WIDTH)
        input("Connect power and press Enter to calibrate...")
        self.pwm(width=self.MAX_WIDTH, snooze=2)  # Official docs: "about 2 seconds".
        self.pwm(
            width=self.MIN_WIDTH, snooze=4
        )  # Time enough for the cell count, etc. beeps to play.
        print("Finished calibration.")

    def arm(self) -> None:
        """
        Arms the ESC. Required upon every power cycle.
        """
        print("Arming...")
        self.pwm(
            width=self.MIN_WIDTH, snooze=4
        )  # Time enough for the cell count, etc. beeps to play.
        print("Armed...")

    def halt(self) -> None:
        """
        Switch of the GPIO, and un-arm the ESC.
        Ensure this runs, even on unclean shutdown.
        """
        print("Slowing...")
        self.pwm(
            width=self.MIN_WIDTH, snooze=1
        )  # This 1 sec seems to *hasten* shutdown.
        print("Failsafe...")
        self.pwm(0)
        self.STOP = True
        print("Disabling GPIO.")
        self.conn.stop()
        print("Halted.")

    def start(self, width=1250) -> None:
        print("start Motor...")
        self.STOP = False
        step = 5  # microseconds
        snooze = 0.3  # seconds
        self.NEW_WIDTH = width
        while True:
            # start_width = self.CURRENT_WIDTH
            # end_width = self.NEW_WIDTH
            width = self.NEW_WIDTH
            print(f"currentSpeed: {self.CURRENT_WIDTH}")
            print(f"newSpeed: {self.NEW_WIDTH}")
            self.pwm(width=width, snooze=snooze)
            self.CURRENT_WIDTH = width

            # if start_width < end_width:
            #     snooze = 0.3  # seconds
            #     print("increase")
            #     for width in range(start_width, end_width + 2, step):
            #         self.pwm(width=width, snooze=snooze)
            #         self.CURRENT_WIDTH = width
            # if self.CURRENT_WIDTH > self.NEW_WIDTH:
            #     snooze = 0.1  # seconds
            #     print("decrease")
            #     for width in range(start_width, end_width - 2, -step):
            #         print("DECREASE")
            #         self.pwm(width=width, snooze=snooze)
            #         self.CURRENT_WIDTH = width
            # if start_width == end_width:
            #     print("continue")
            #     snooze = 0.3  # seconds
            #     self.pwm(width=end_width, snooze=snooze)
            if self.STOP:
                print("stop")
                self.halt()
                break

    def test(self) -> None:
        global tes
        """
        Test with a triangularish wave.
        Useful for determining the max and min PWM values.
        """
        # # max_width = self.MAX_WIDTH - 700  # microseconds
        # max_width = 1150  # microseconds
        # min_width = 1100  # microseconds
        # step = 10  # microseconds


        # # input("Press Enter to conduct run-up test...")
        # for i in range(1):
        #     print("Increasing...")
        #     for width in range(min_width, max_width, step):
        #         self.pwm(width=width, snooze=snooze)

        # sleep(1)  # Duration test.  Seems to last almost 60s @ 1350.

        # print("Holding at max...")
        # snooze = 0.1
        # print("Decreasing...")
        # for width in range(max_width, min_width, -step):
        #     self.pwm(width=width, snooze=snooze)

    def test_Long(self) -> None:
        """
        Test with a triangularish wave.
        Useful for determining the max and min PWM values.
        """
        # max_width = self.MAX_WIDTH - 700  # microseconds
        max_width = self.MAX_WIDTH  # microseconds
        min_width = self.MIN_WIDTH + 80  # microseconds
        step = 10  # microseconds
        snooze = 0.3  # seconds
        for i in range(1):
            print("Increasing...")
            for width in range(min_width, max_width, step):
                self.pwm(width=width, snooze=snooze)
                self.currentSpeed = width

        sleep(1)  # Duration test.  Seems to last almost 60s @ 1350.
        input("Press Enter to stop test...")
        print("Holding at max...")
        snooze = 0.1
        print("Decreasing...")
        for width in range(max_width, min_width, -step):
            self.pwm(width=width, snooze=snooze)
            self.currentSpeed = width


# --------------------------------------------------------------


# Init:
esc1 = ESC(pin=22, name="motor3")
esc2 = ESC(pin=4, name="motor1")
esc3 = ESC(pin=23, name="motor4")
esc4 = ESC(pin=27, name="motor2")


def calibrate_ESC(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        esc1.calibrate()
        print(f"calibrate ESC1..")

    if motor2:
        esc2.calibrate()
        print(f"calibrate ESC2..")
    if motor3:
        esc3.calibrate()
        print(f"calibrate ESC3..")
    if motor4:
        esc4.calibrate()
        print(f"calibrate ESC4..")

    print("calibration ended")


def arming(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        thread = Thread(target=esc1.arm)
        try:
            thread.start()
            print("motor1 arm")
        except Exception:
            print("Motor1 arm fail")

    if motor2:
        thread = Thread(target=esc2.arm)
        try:
            thread.start()
            print("motor2 arm")
        except Exception:
            print("Motor2 arm fail")
    if motor3:
        thread = Thread(target=esc3.arm)
        try:
            thread.start()
            print("motor3 arm")
        except Exception:
            print("Motor3 arm fail")
    if motor4:
        thread = Thread(target=esc4.arm)
        try:
            thread.start()
            print("motor4 arm")
        except Exception:
            print("Motor4 arm fail")

    print("Arming ended")


def run(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        thread = Thread(target=esc1.start)
        try:
            thread.start()
            print("motor1 launch")
        except Exception:
            print("Motor1 lunch fail")

    if motor2:
        thread = Thread(target=esc2.start)
        try:
            thread.start()
            print("motor2 launch")
        except Exception:
            print("Motor2 lunch fail")
    if motor3:
        thread = Thread(target=esc3.start)
        try:
            thread.start()
            print("motor3 launch")
        except Exception:
            print("Motor3 lunch fail")
    if motor4:
        thread = Thread(target=esc4.start)
        try:
            thread.start()
            print("motor4 launch")
        except Exception:
            print("Motor4 lunch fail")


def halt(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        halt_motor1 = Thread(target=esc1.halt)
        halt_motor1.start()
        print("motors halt launch")
    if motor2:
        halt_motor2 = Thread(target=esc2.halt)
        halt_motor2.start()
        print("motors halt launch")
    if motor3:
        halt_motor3 = Thread(target=esc3.halt)
        halt_motor3.start()
        print("motors halt launch")
    if motor4:
        halt_motor4 = Thread(target=esc4.halt)
        print("motors halt launch")
        halt_motor4.start()


## TEST ##


# # halt(motor1=True,motor2=False,motor3=False,motor4=False)

# try:
#     print("test")
#     # calibrate_ESC(motor1=False, motor2=False, motor3=True, motor4=False)
#     arming(motor1=True, motor2=True, motor3=True, motor4=True)
#     sleep(10)
#     run(motor1=True, motor2=True, motor3=True, motor4=True)
#     sleep(40)
#     halt(motor1=True, motor2=True, motor3=True, motor4=True)
#     print("endtest")
# except KeyboardInterrupt:
#     halt(motor1=True, motor2=True, motor3=True, motor4=True)
# finally:
#     pass
# halt(motor1=True,motor2=True,motor3=True,motor4=True)
