from accelerometer import GetData, AccelerationData
from time import sleep
from threading import Thread
import brushlessManager
from ledManager import turnOn_thread,led3

stabiliseThread_Stop = False

def Stabilise():
    print("Stabilise Thread is running..")
    global stabiliseThread_Stop
    accelerometer_Thread = Thread(target=GetData)
    accelerometer_Thread.start()
    sleep(5)
    turnOn_thread(led3,True,"G",0,0.5)
    while True:
        # print("data:")
        if stabiliseThread_Stop == True:
            turnOn_thread(led3,False,"R")
            print("Stabilise Thread is down..")
            break
        x = AccelerationData["x"]
        y = AccelerationData["y"]
        z = AccelerationData["z"]
        orientation = AccelerationData["orientation"]
        print(f"x:{x},y:{y}")
        sleep(0.1)
        if x > 1.5:
            print("x+1")
            # if brushlessManager.esc1.CURRENT_WIDTH >= brushlessManager.esc1.MIN_WIDTH +100:
            print("x PowerUp")
            brushlessManager.esc1.NEW_WIDTH = 1280
            brushlessManager.esc2.NEW_WIDTH = 1280
            brushlessManager.esc3.NEW_WIDTH = 1200
            brushlessManager.esc4.NEW_WIDTH = 1200

        if x < -1.5:
            print("x-1")
            # if brushlessManager.esc1.CURRENT_WIDTH <= brushlessManager.esc1.MAX_WIDTH -100:
            print("x PowerDown")
            brushlessManager.esc1.NEW_WIDTH = 1200
            brushlessManager.esc2.NEW_WIDTH = 1200
            brushlessManager.esc3.NEW_WIDTH = 1280
            brushlessManager.esc4.NEW_WIDTH = 1280
        if y < 1.5:
            # if brushlessManager.esc1.CURRENT_WIDTH >= brushlessManager.esc1.MIN_WIDTH +100:
            print("y+1")
            print("y PowerUp")
            brushlessManager.esc1.NEW_WIDTH = 1200
            brushlessManager.esc2.NEW_WIDTH = 1280
            brushlessManager.esc3.NEW_WIDTH = 1280
            brushlessManager.esc4.NEW_WIDTH = 1200
        if y > -1.5:
            # if brushlessManager.esc1.CURRENT_WIDTH <= brushlessManager.esc1.MIN_WIDTH +100:
            print("y-1")
            print("y PowerDown")
            brushlessManager.esc1.NEW_WIDTH = 1280
            brushlessManager.esc2.NEW_WIDTH = 1200
            brushlessManager.esc3.NEW_WIDTH = 1200
            brushlessManager.esc4.NEW_WIDTH = 1280
        if -1.5 < x < 1.5 and -1.5 < y < 1.5:
            print("stable")
            brushlessManager.esc1.NEW_WIDTH = 1280
            brushlessManager.esc2.NEW_WIDTH = 1280
            brushlessManager.esc3.NEW_WIDTH = 1280
            brushlessManager.esc4.NEW_WIDTH = 1280
