# Algo reconnassance Faciale avec OpenCV et Face_recognition
# Pajak Alexandre 2021
# -- coding: utf-8 --

from re import T
from threading import Thread
import face_recognition
import cv2
import numpy as np
from time import sleep

from ledManager import turnOn_thread, led2, led4
import brushlessManager

# shared var:
faceCoords: tuple = (1, 1)
turnOff_faceDetection = False
direction: str
direction = "good"
# globals:
detected: bool = False
takePic: bool = False

def Detection():
    print("Face Detection Thread is running..")
    global turnOff_faceDetection
    global detected
    global faceCoords
    global takePic
    # récupération du flux vidéo webcam
    video_capture = cv2.VideoCapture(0)
    # chargement de la base de donnée depuis fichier image
    if takePic == True:
        turnOn_thread(led4, False, "G")
        cv2.imwrite("images/image.jpg")
        sleep(1)
        takePic = False
        turnOn_thread(led4, False, "R")
    obama_image = face_recognition.load_image_file("images/obama.jpg")
    alex_image = face_recognition.load_image_file("images/moi.jpg")
    obama_face_encoding = face_recognition.face_encodings(obama_image)[0]
    alex_face_encoding = face_recognition.face_encodings(alex_image)[0]

    # correspondance visage connu&nom
    known_face_encodings = [
        obama_face_encoding,
        alex_face_encoding,
    ]
    known_face_names = [
        "Obama",
        "Alex",
    ]

    def isDetected():
        if detected == True:
            turnOn_thread(led2, False, "G")
        else:
            turnOn_thread(led2, False, "B")

    # variables pour detection
    face_locations = []
    face_encodings = []
    face_names = []
    process_this_frame = True
    print("Face_Recognition start...")
    while True:
        # capture image
        ret, frame = video_capture.read()

        # for drone:
        (h, w) = frame.shape[:2]
        (cX, cY) = (w // 2, h // 2)
        # rotate our image by 90 degrees around the image 270 for revers test
        M = cv2.getRotationMatrix2D((cX, cY), 90, 1.0)
        rotated = cv2.warpAffine(frame, M, (w, h))

        # downScale
        small_frame = cv2.resize(rotated, (0, 0), fx=0.25, fy=0.25)
        # convertion BGR (OpenCV) a RGB (face_recognition)
        rgb_small_frame = small_frame[:, :, ::-1]

        if process_this_frame:
            # trouvé les visages
            face_locations = face_recognition.face_locations(rgb_small_frame)
            face_encodings = face_recognition.face_encodings(
                rgb_small_frame, face_locations
            )

            face_names = []
            for face_encoding in face_encodings:
                detected = True
                # comparaison avec visage connu
                # matches = face_recognition.compare_faces(
                #     known_face_encodings, face_encoding
                # )
                name = "Inconnu "
                face_distances = face_recognition.face_distance(
                    known_face_encodings, face_encoding
                )
                best_match_index = np.argmin(face_distances)
                # if matches[best_match_index]:
                #     name = known_face_names[best_match_index]

                face_names.append(name)
        else:
            detected = False
            faceCoords = (0,0)
        isDetected()
        process_this_frame = not process_this_frame

        # affichage
        # if len(face_names) == 0:
        #     print("RAS")
        # else:
        #     for name in face_names:
        #         if name == "Inconnu ":
        #             print("Inconnu détecter")
        #         else:
        #             print("Bonjour " + name + " !")
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # retour format initial
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4
            # shared tuple
            faceCoords = (left, top)
            # print(faceCoords)

        ### For window visualisation ###
        #####################################################################################
        #     # carré rouge
        #     cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)
        #     # coords of head square for drone oriantation

        #     # nom
        #     cv2.rectangle(
        #         frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED
        #     )
        #     font = cv2.FONT_HERSHEY_DUPLEX
        #     cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        # cv2.imshow("Video", frame)
        #####################################################################################
        # exit
        if turnOff_faceDetection:
            # stop webcam
            video_capture.release()
            turnOn_thread(led2, color="R")
            print("Face Detection Thread down..")
            # cv2.destroyAllWindows()
            break


def takePics():
    turnOn_thread(led4, True, "R", 3, 0.5)
    global takePic
    takePic = True
    print("Smile for the pic !")


def keepOnTarget():
    global direction
    global faceCoords
    global direction
    faceDetection_Thread = Thread(target=Detection)
    faceDetection_Thread.start()
    # left max = 0, right max = 1280 top max 0, bottom max = 720
    margin = 20
    x_max: int = 350
    x_min: int = 160
    y_max: int = 244
    y_min: int = 0
    while True:
        lastCoords = (0,0)
        if faceCoords != (0,0):
            lastCoords = faceCoords
            coords = faceCoords
            print(f"x:{coords[0]} y:{coords[1]}")
        # if coords[0] > x_max - margin:
        #     direction = "right turn"
        #     print("right turn")
        # if coords[0] < x_min + margin:
        #     direction = "left turn"
        #     print("left turn")
        # if coords[2] > y_max - margin:
        #     direction = "decrease"
        #     print("decrease")
        # if coords[2] < y_min + margin:
        #     direction = "increase"
        #     print("increase")
        # else:
        #     direction = "good"
        # print(f"direction: {direction}")
#keepOnTarget()