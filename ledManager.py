from threading import Thread
try:
    import RPi.GPIO as GPIO
except Exception:
    print("RPi.GPIO import fail.")
from time import sleep
import datetime


class Led:
    def __init__(self, RED: int, GREEN: int, BLUE: int, name: str):
        self.RED = RED
        self.GREEN = GREEN
        self.BLUE = BLUE
        self.name = name
        self.turn0ff_thread = False
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.RED, GPIO.OUT)
        GPIO.output(self.RED, 0)
        GPIO.setup(self.GREEN, GPIO.OUT)
        GPIO.output(self.GREEN, 0)
        GPIO.setup(self.BLUE, GPIO.OUT)
        GPIO.output(self.BLUE, 0)

    def start(self):
        self.turn0ff_thread = False
        GPIO.setmode(GPIO.BCM)
        GPIO.setwarnings(False)
        GPIO.setup(self.RED, GPIO.OUT)
        GPIO.output(self.RED, 0)
        GPIO.setup(self.GREEN, GPIO.OUT)
        GPIO.output(self.GREEN, 0)
        GPIO.setup(self.BLUE, GPIO.OUT)
        GPIO.output(self.BLUE, 0)

    def turnOn_red(self, duration_seconds: int = 0):
        endTime = datetime.datetime.now() + datetime.timedelta(0, duration_seconds)
        self.start()
        while True:
            if duration_seconds > 0:
                if datetime.datetime.now() <= endTime:
                    GPIO.output(self.RED, 1)
                    GPIO.output(self.GREEN, 0)
                    GPIO.output(self.BLUE, 0)
                else:
                    self.turn0ff()
                    break
            else:
                if self.turn0ff_thread:
                    self.turn0ff()
                    break
                else:
                    GPIO.output(self.RED, 1)
                    GPIO.output(self.GREEN, 0)
                    GPIO.output(self.BLUE, 0)

    def turnOn_green(self, duration_seconds: int = 0):
        endTime = datetime.datetime.now() + datetime.timedelta(0, duration_seconds)
        self.start()
        while True:
            if duration_seconds > 0:
                if datetime.datetime.now() <= endTime:
                    GPIO.output(self.RED, 0)
                    GPIO.output(self.GREEN, 1)
                    GPIO.output(self.BLUE, 0)
                else:
                    self.turn0ff()
                    break
            else:
                if self.turn0ff_thread:
                    self.turn0ff()
                    break
                else:
                    GPIO.output(self.RED, 0)
                    GPIO.output(self.GREEN, 1)
                    GPIO.output(self.BLUE, 0)

    def turnOn_blue(self, duration_seconds: int = 0):
        endTime = datetime.datetime.now() + datetime.timedelta(0, duration_seconds)
        self.start()
        while True:
            if duration_seconds > 0:
                if datetime.datetime.now() <= endTime:
                    GPIO.output(self.RED, 0)
                    GPIO.output(self.GREEN, 0)
                    GPIO.output(self.BLUE, 1)
                else:
                    self.turn0ff()
                    break
            else:
                if self.turn0ff_thread:
                    self.turn0ff()
                    break
                else:
                    GPIO.output(self.RED, 0)
                    GPIO.output(self.GREEN, 0)
                    GPIO.output(self.BLUE, 1)

    def blinking(self, rate: float = 0.5, color: str = "R", duration_seconds: int = 0):
        endTime = datetime.datetime.now() + datetime.timedelta(0, duration_seconds)
        self.start()
        while True:
            sleep(rate)
            if duration_seconds > 0:
                if datetime.datetime.now() <= endTime:
                    if color == "R":
                        GPIO.output(self.RED, 1)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                        sleep(rate)
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                    if color == "G":
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 1)
                        GPIO.output(self.BLUE, 0)
                        sleep(rate)
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                    if color == "B":
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 1)
                        sleep(rate)
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                else:
                    # self.turn0ff()
                    break
            else:
                if self.turn0ff_thread:
                    self.turn0ff()
                    break
                else:
                    if color == "R":
                        GPIO.output(self.RED, 1)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                        sleep(rate)
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                    if color == "G":
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 1)
                        GPIO.output(self.BLUE, 0)
                        sleep(rate)
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)
                    if color == "B":
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 1)
                        sleep(rate)
                        GPIO.output(self.RED, 0)
                        GPIO.output(self.GREEN, 0)
                        GPIO.output(self.BLUE, 0)

    def turn0ff(self):
        # self.start()
        GPIO.output(self.RED, 0)
        GPIO.output(self.GREEN, 0)
        GPIO.output(self.BLUE, 0)
        self.close_GPIO

    def close_GPIO():
        GPIO.cleanup()


def turnOn_thread(
    led: Led,
    blinking = False,
    color: str = "R",
    duration_seconds: int = 0,
    rate: float = 0.5,
):
    turnOff_thread(led)
    if blinking:
        thread = Thread(
            target=led.blinking,
            args=(
                rate,
                color,
                duration_seconds,
            ),
        )
        thread.start()
        # print(f"blinking thread {led.name} start")
    else:
        if color == "R":
            thread = Thread(target=led.turnOn_red, args=(duration_seconds,))
            thread.start()
            # print(f"thread {led.name} start")
        if color == "G":
            thread = Thread(target=led.turnOn_green, args=(duration_seconds,))
            thread.start()
            # print(f"thread {led.name} start")
        if color == "B":
            thread = Thread(target=led.turnOn_blue, args=(duration_seconds,))
            thread.start()
            # print(f"thread {led.name} start")


def turnOff_thread(led: Led):
    led.turn0ff_thread = True


led4 = Led(9, 11, 10, "led1")
led2 = Led(1, 8, 7, "led2")
led1 = Led(16, 20, 21, "led3")
led3 = Led(6, 5, 13, "led4")


## TEST ##
#################################################
print("test Led")

#turnOn_thread(led1,False,"R",0,0)
# sleep(1)
# turnOn_thread(led1,False,"B",0,0)
# sleep(1)
# turnOn_thread(led1,False,"G",0,0)
# sleep(1)
# turnOn_thread(led1,True,"R",0,1)
# sleep(5)
#turnOn_thread(led1,False,"B",0,0)
# turnOff_thread(led1)
# turnOff_thread(led2)
# turnOff_thread(led3)
# turnOff_thread(led4)

## END TEST ##
#################################################
