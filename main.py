## Ypets PROJECT ##
## Main code ##
# Alexandre Pajak 17/06/2022
# -- coding: utf-8 --

# import:
from time import sleep
from threading import Thread
from ledManager import turnOn_thread, led1, led2, led3, led4
import os

## Files import:
from faceDetection import keepOnTarget, takePics
from servoManager import store_arms, deploy_arms, stop_servos, init_servos
import sys
import brushlessManager
from stabilise import Stabilise

# global:

### INIT ################
init_servos()
turnOn_thread(led1, True, "R", 2, 0.2)
turnOn_thread(led2, True, "R", 2, 0.2)
turnOn_thread(led3, True, "R", 2, 0.2)
turnOn_thread(led4, True, "R", 2, 0.2)
sleep(2)
turnOn_thread(led1, False, "G")
turnOn_thread(led2, False, "R")

#########################
def main():
    deploy_arms()
    print("arm deployement")
    sleep(5)
    brushlessManager.arming(motor1=True, motor2=True, motor3=True, motor4=True)
    print("run motors in 10")
    sleep(10)
    brushlessManager.run(motor1=True, motor2=True, motor3=True, motor4=True)
    stabilise_Thread = Thread(target=Stabilise)
    stabilise_Thread.start()
    print("stabilise thread started..")
    keepOnTarget_Thread = Thread(target=keepOnTarget)
    keepOnTarget_Thread.start()
    print("keepOnTarget thread started..")
    sleep(20)
    takePics()
    sleep(40)
    brushlessManager.halt(motor1=True, motor2=True, motor3=True, motor4=True)
    sleep(10)
    store_arms()


if __name__ == "__main__":
    try:
        print("mainLaunch..")
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        sleep(5)
        store_arms()
        brushlessManager.halt(motor1=True, motor2=True, motor3=True, motor4=True)
        stop_servos()
        os.system("sudo killall pigpiod")
        sleep(2)
        sys.exit(0)
