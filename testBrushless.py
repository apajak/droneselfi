#!/usr/bin/env python3

from threading import Thread
import time
import os
from time import sleep

try:
    import pigpio  # importing GPIO library
except Exception:
    print("pigpio module error")

try:
    os.system("sudo killall pigpiod")
except Exception:
    print("sudo killall error")
try:
    os.system("sudo pigpiod")  # Launching GPIO library
except Exception:
    print("sudo pigpiod error")
time.sleep(1)


class TurnigyESC:
    """
    Turnigy Electonic Speed controller.
    Tested with:
      Plush 30 A
    """

    ## YMMV
    MIN_WIDTH = 650

    # Note that some batteries, 12 volt PSUs, etc. might only be capable of far less than this (e.g. 1350)
    # However, the controllers range should still be set to max for finest full-scale resolution.
    # MAX_WIDTH = 2400
    MAX_WIDTH = 1350
    NewSpeed = 1350

    def __init__(self, pin, name):
        self.conn = pigpio.pi()
        self.pin = pin
        self.name = name
        self.currentSpeed = 0

    def pwm(self, width: int, snooze: int = 0) -> None:
        """
        Convenience wrapper around pigpio's set_servo_pulsewidth.
        width: The servo pulsewidth in microseconds. 0 switches pulses off.
        snooze: seconds before returning.
        """
        print("Setting pulse width to", width, "microseconds for", snooze, "seconds.")
        self.conn.set_servo_pulsewidth(self.pin, width)
        if snooze:
            time.sleep(snooze)
        return

    def calibrate(self) -> None:
        """
        This trains the ESC on the full scale (max - min range) of the controller / pulse generator.
        This only needs to be done when changing controllers, transmitters, etc. not upon every power-on.
        NB: if already calibrated, full throttle will be applied (briefly)!  Disconnect propellers, etc.
        """
        print("Calibrating...")
        self.pwm(width=self.MAX_WIDTH)
        input("Connect power and press Enter to calibrate...")
        self.pwm(width=self.MAX_WIDTH, snooze=2)  # Official docs: "about 2 seconds".
        self.pwm(
            width=self.MIN_WIDTH, snooze=4
        )  # Time enough for the cell count, etc. beeps to play.
        print("Finished calibration.")

    def arm(self) -> None:
        """
        Arms the ESC. Required upon every power cycle.
        """
        print("Arming...")
        self.pwm(
            width=self.MIN_WIDTH, snooze=4
        )  # Time enough for the cell count, etc. beeps to play.
        print("Armed...")

    def halt(self) -> None:
        """
        Switch of the GPIO, and un-arm the ESC.
        Ensure this runs, even on unclean shutdown.
        """
        print("Slowing...")
        self.pwm(
            width=self.MIN_WIDTH, snooze=1
        )  # This 1 sec seems to *hasten* shutdown.
        print("Failsafe...")
        self.pwm(0)
        print("Disabling GPIO.")
        self.conn.stop()
        print("Halted.")

    def test(self) -> None:
        """
        Test with a triangularish wave.
        Useful for determining the max and min PWM values.
        """
        # max_width = self.MAX_WIDTH - 700  # microseconds
        max_width = self.MAX_WIDTH - 300  # microseconds
        min_width = self.MIN_WIDTH + 80  # microseconds
        step = 9  # microseconds

        snooze = 0.3  # seconds

        # input("Press Enter to conduct run-up test...")
        for i in range(1):
            print("Increasing...")
            for width in range(min_width, max_width, step):
                self.pwm(width=width, snooze=snooze)
                self.currentSpeed = width

        time.sleep(1)  # Duration test.  Seems to last almost 60s @ 1350.

        print("Holding at max...")
        snooze = 0.1
        print("Decreasing...")
        for width in range(max_width, min_width, -step):
            self.pwm(width=width, snooze=snooze)
            self.currentSpeed = width

    def test_Long(self) -> None:
        """
        Test with a triangularish wave.
        Useful for determining the max and min PWM values.
        """
        # max_width = self.MAX_WIDTH - 700  # microseconds
        max_width = self.MAX_WIDTH - 400  # microseconds
        min_width = self.MIN_WIDTH + 80  # microseconds
        step = 9  # microseconds
        snooze = 0.3  # seconds
        for i in range(1):
            print("Increasing...")
            for width in range(min_width, max_width, step):
                self.pwm(width=width, snooze=snooze)
                self.currentSpeed = width

        time.sleep(1)  # Duration test.  Seems to last almost 60s @ 1350.
        input("Press Enter to stop test...")
        print("Holding at max...")
        snooze = 0.1
        print("Decreasing...")
        for width in range(max_width, min_width, -step):
            self.pwm(width=width, snooze=snooze)
            self.currentSpeed = width


# Init:
esc1 = TurnigyESC(pin=4, name="motor1")
esc2 = TurnigyESC(pin=27, name="motor2")
esc3 = TurnigyESC(pin=22, name="motor3")  # dead
esc4 = TurnigyESC(pin=23, name="motor4")


def calibrate(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        calibration_motor1 = Thread(target=esc1.calibrate)
        calibration_motor1.start()
        calibration_motor1.join()
        print(f"calibrate launch")

    if motor2:
        calibration_motor2 = Thread(target=esc2.calibrate)
        calibration_motor2.start()
        print(f"calibrate launch")
        calibration_motor2.join()
    if motor3:
        calibration_motor3 = Thread(target=esc3.calibrate)
        calibration_motor3.start()
        print(f"calibrate launch")
        calibration_motor3.join()
    if motor4:
        calibration_motor4 = Thread(target=esc4.calibrate)
        calibration_motor4.start()
        print(f"calibrate launch")
        calibration_motor4.join()
    print("calibrate ended")


def arming(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        arming_motor1 = Thread(target=esc1.arm)
        arming_motor1.start()
        arming_motor1.join()
        print("arming launch")

    if motor2:
        arming_motor2 = Thread(target=esc2.arm)
        arming_motor2.start()
        arming_motor2.join()
        print("arming launch")

    if motor3:
        arming_motor3 = Thread(target=esc3.arm)
        arming_motor3.start()
        arming_motor3.join()
        print("arming launch")

    if motor4:
        arming_motor4 = Thread(target=esc4.arm)
        arming_motor4.start()
        arming_motor4.join()
        print("arming launch")


def test(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        test_motor1 = Thread(target=esc1.test)
        test_motor1.start()
        test_motor1.join()
        print("motors test launch")

    if motor2:
        test_motor2 = Thread(target=esc2.test)
        test_motor2.start()
        test_motor2.join()
        print("motors test launch")

    if motor3:
        test_motor3 = Thread(target=esc3.test)
        test_motor3.start()
        test_motor3.join()
        print("motors test launch")

    if motor4:
        test_motor4 = Thread(target=esc4.test)
        test_motor4.start()
        test_motor4.join()
        print("motors test launch")


def test_Long(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        test_motor1 = Thread(target=esc1.test_Long)
        test_motor1.start()
        # test_motor1.join()
        print("motors test launch")

    if motor2:
        test_motor2 = Thread(target=esc2.test_Long)
        test_motor2.start()
        test_motor2.join()
        print("motors test launch")

    if motor3:
        test_motor3 = Thread(target=esc3.test_Long)
        test_motor3.start()
        test_motor3.join()
        print("motors test launch")

    if motor4:
        test_motor4 = Thread(target=esc4.test_Long)
        test_motor4.start()
        test_motor4.join()
        print("motors test launch")


def halt(
    motor1: bool = True, motor2: bool = True, motor3: bool = True, motor4: bool = True
):
    if motor1:
        halt_motor1 = Thread(target=esc1.halt)
        halt_motor1.start()
        halt_motor1.join()
        print("motors halt launch")
    if motor2:
        halt_motor2 = Thread(target=esc2.halt)
        halt_motor2.start()
        halt_motor2.join()
        print("motors halt launch")
    if motor3:
        halt_motor3 = Thread(target=esc3.halt)
        halt_motor3.start()
        halt_motor3.join()
        print("motors halt launch")
    if motor4:
        halt_motor4 = Thread(target=esc4.halt)
        print("motors halt launch")
        halt_motor4.start()
        halt_motor4.join()


if __name__ == "__main__":

    try:
        print("arm ESC1..")
        esc1.arm()
        sleep(3)
        print("Test BaT Launch..")
        esc1.test_Long()
        # calibrate(False, False, False, False)
        # arming(True, False, False, False)
        # test_Long(True, False, False, False)

    except KeyboardInterrupt:
        pass
    finally:
        esc1.halt()
        # halt(True, False, False, False)
