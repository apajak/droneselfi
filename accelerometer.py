## Ypets PROJECT ##
## code for manage adafruit MMA8451 3 axis accelerometer ##
# Alexandre Pajak 09/06/2022
# -- coding: utf-8 --


import time

try:
    import board
    import adafruit_mma8451
except Exception:
    print("import error !")

# Connect to Rasp :
# - VIN : 1
# - GND : 9
# - SCL : 5
# - SDA : 3


# Create sensor object, communicating over the board's default I2C bus
try:
    i2c = board.I2C()  # uses board.SCL and board.SDA
except Exception:
    print("error i2c")
time.sleep(2)
# Initialize MMA8451 module.
try:
    sensor = adafruit_mma8451.MMA8451(i2c)
except Exception:
    print("sensor not init")
AccelerationData = {"x": float, "y": float, "z": float, "orientation": str}
# Main loop to print the acceleration and orientation every second.
def GetData():
    global AccelerationData
    while True:
        x, y, z = sensor.acceleration
        print(
            "Acceleration: x={0:0.3f}m/s^2 y={1:0.3f}m/s^2 z={2:0.3f}m/s^2".format(
                x, y, z
            )
        )
        AccelerationData["x"] = x
        AccelerationData["y"] = y
        AccelerationData["z"] = z

        orientation = sensor.orientation

        # print("Orientation: ", end="")
        if orientation == adafruit_mma8451.PL_PUF:
            AccelerationData["orientation"] = "Portrait, up, front"
            # print("Portrait, up, front")
        elif orientation == adafruit_mma8451.PL_PUB:
            AccelerationData["orientation"] = "Portrait, up, back"
            # print("Portrait, up, back")
        elif orientation == adafruit_mma8451.PL_PDF:
            AccelerationData["orientation"] = "Portrait, down, front"
            # print("Portrait, down, front")
        elif orientation == adafruit_mma8451.PL_PDB:
            AccelerationData["orientation"] = "Portrait, down, back"
            # print("Portrait, down, back")
        elif orientation == adafruit_mma8451.PL_LRF:
            AccelerationData["orientation"] = "Landscape, right, front"
            # print("Landscape, right, front")
        elif orientation == adafruit_mma8451.PL_LRB:
            AccelerationData["orientation"] = "Landscape, right, back"
            # print("Landscape, right, back")
        elif orientation == adafruit_mma8451.PL_LLF:
            AccelerationData["orientation"] = "Landscape, left, front"
            # print("Landscape, left, front")
        elif orientation == adafruit_mma8451.PL_LLB:
            AccelerationData["orientation"] = "Landscape, left, back"
            # print("Landscape, left, back")
        time.sleep(0.2)


#GetData()
