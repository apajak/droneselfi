## Ypets PROJECT ##
## code for detect position of hands, bodys, faces  & face recognition ##
# Alexandre Pajak 17/06/2022
# -- coding: utf-8 --

import face_recognition
import cv2
import numpy as np
import mediapipe as mp
import time
import handTrackingVolume

# récupération du flux vidéo webcam
video_capture = cv2.VideoCapture(0)

# hand Detection Var:
pTime = 0
cTime = 0
hanDetector = handTrackingVolume.handDetector()

# body detection Var :
mpPose = mp.solutions.pose
pose = mpPose.Pose()
mpDraw = mp.solutions.drawing_utils

# chargement de la base de donnée depuis fichier image
obama_image = face_recognition.load_image_file("images/obama.jpg")
alex_image = face_recognition.load_image_file("images/moi.jpg")
loriane_image = face_recognition.load_image_file("images/loriane.png")
obama_face_encoding = face_recognition.face_encodings(obama_image)[0]
alex_face_encoding = face_recognition.face_encodings(alex_image)[0]
loriane_face_encoding = face_recognition.face_encodings(loriane_image)[0]

# correspondance visage connu&nom
known_face_encodings = [
    obama_face_encoding,
    alex_face_encoding,
    loriane_face_encoding,
]
known_face_names = [
    "Obama",
    "Alex",
    "Loriane"
]

# variables pour detection
face_locations = []
face_encodings = []
face_names = []
process_this_frame = True

while True:
    # capture image
    ret, frame = video_capture.read()
    # success, img = cap.read()

    # hand Detection:
    img = hanDetector.findHands(frame)
    lmlist = hanDetector.findPosition(frame)
    if len(lmlist) != 0:
        print(lmlist[4])
    cTime = time.time()
    fps = 1 / (cTime - pTime)
    pTime = cTime

    # body detection:
    imgRGB = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    results = pose.process(imgRGB)
    print(results.pose_landmarks)
    if results.pose_landmarks:
        mpDraw.draw_landmarks(frame, results.pose_landmarks, mpPose.POSE_CONNECTIONS)
        for id, lm in enumerate(results.pose_landmarks.landmark):
            h, w, c = frame.shape
            print(id, lm)
            cx, cy = int(lm.x * w), int(lm.y * h)

    # downScale
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

    # convertion BGR (OpenCV) a RGB (face_recognition)
    rgb_small_frame = small_frame[:, :, ::-1]

    if process_this_frame:
        # trouvé les visages
        face_locations = face_recognition.face_locations(rgb_small_frame)
        face_encodings = face_recognition.face_encodings(
            rgb_small_frame, face_locations
        )

        face_names = []
        for face_encoding in face_encodings:
            # comparaison avec visage connu
            matches = face_recognition.compare_faces(
                known_face_encodings, face_encoding
            )
            name = "Inconnu "
            face_distances = face_recognition.face_distance(
                known_face_encodings, face_encoding
            )
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = known_face_names[best_match_index]

            face_names.append(name)

    process_this_frame = not process_this_frame

    # affichage
    if len(face_names) == 0:
        pass
        # print("RAS")
    else:
        for name in face_names:
            if name == "Inconnu ":
                # print("intrus détecter !")
                pass
            else:
                pass
                # print("Bonjour " + name + " !")
    for (top, right, bottom, left), name in zip(face_locations, face_names):
        # retour format initial
        top *= 4
        right *= 4
        bottom *= 4
        left *= 4

        # carré rouge
        cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

        # nom
        cv2.rectangle(
            frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED
        )
        font = cv2.FONT_HERSHEY_DUPLEX
        cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)

        cv2.putText(
            frame, str(int(fps)), (10, 70), cv2.FONT_HERSHEY_PLAIN, 3, (255, 0, 0), 3
        )
        cv2.circle(frame, (cx, cy), 5, (255, 0, 0), cv2.FILLED)

    cv2.imshow("Video", frame)

    # exit
    if cv2.waitKey(1) & 0xFF == ord("q"):
        break

# stop webcam
video_capture.release()
cv2.destroyAllWindows()
