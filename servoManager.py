from time import sleep
from piservo import Servo
import os

try:
    os.system("sudo killall pigpiod")
except Exception:
    print("sudo killall error")
try:
    os.system("sudo pigpiod")  # Launching GPIO library
except Exception:
    print("sudo pigpiod error")

sleep(1)
servo = Servo(18)


def init_servos():
    print("start")
    servo.start()
    servo.write(0)


def deploy_arms():
    print("deploy")
    servo.write(145)


def store_arms():
    print("store")
    servo.write(0)


def stop_servos():
    print("stop")
    servo.stop()


## Test SERVOS ##
#######################################

init_servos()
# print("start servos Test")
# sleep(1)
# deploy_arms()
# sleep(3)
# store_arms()
# sleep(1)
# stop_servos()
# print("end servo test")

## END TEST SERVOS ##
#######################################
